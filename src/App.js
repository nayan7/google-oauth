import React from 'react';
import GoogleLogin from 'react-google-login';

const responseGoogle = response => {
  console.log(response);
};
function App() {
  return (
    <div className="App">
      <GoogleLogin
        clientId='1040906393010-cdnpiitkcv3jvnm2o1jfatq4p0llf1qu.apps.googleusercontent.com'
        buttonText='Google'
        onSuccess={responseGoogle}
        onFailure={responseGoogle}

      
       />

    </div>
  );
}

export default App
